package TestNG;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ScheduleFlight {
	WebDriver driver;			
	public Logger log;
    ExtentReports extent;
	ExtentHtmlReporter reporter;
	
	@BeforeSuite
	public void setup() {
   		
   		reporter = new ExtentHtmlReporter("./reports/results.html");
   		
   		extent = new ExtentReports();
		
		extent.attachReporter(reporter);
   
	}
	@BeforeTest			
	public void setupTest()			
	{			
		System.setProperty("webdriver.chrome.driver", "C:\\Users\\Neha\\Downloads\\chromedriver_win32\\chromedriver.exe");		
		driver = new ChromeDriver();		
				
	}			
				
	@AfterTest			
	public void tearDownTest()			
	{			
		driver.close();		
				
	}	
	
	@Test
	public void adminlogintest() {
		
		// Starts Report
		
		ExtentTest logger =	extent.createTest("TestAdmin_LogIn","Login to the admin page");
		
		driver.navigate().to("http://43.254.161.195:8085/happytripcrclean1/loginAdmin.html");
		
		driver.findElement(By.id("username")).sendKeys("admin@mindtree.com");
		driver.findElement(By.id("password")).sendKeys("admin");
        File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		try {
			FileUtils.copyFile(src, new File("C://Users//Neha//eclipse-workspace//Automation//AutomationHappyTrip//screenshots//login.png"));
			System.out.println("Test case passed");
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
		driver.findElement(By.xpath("//*[@id=\"signInButton\"]")).click();
	
		logger.log(Status.PASS, "Admin LoggedIn");
		logger.log(Status.INFO, "LogIn to Admin");
		
		
		extent.flush();
	
		
		
	}
	
	@Test(description="This test is for testing schedule Flight")
	public void scheduleFlight() throws InterruptedException {
		
	
		ExtentTest logger1 = extent.createTest("Schedule Flight","Login to the schedule Flight page");
		
		//TestCase1 
        driver.findElement(By.linkText(("Schedule Flight"))).click();
        logger1.log(Status.PASS, "Schedule Flight navigated");
        logger1.log(Status.INFO, "Clicked to Schedule Flight");
		
		//TestCase2 :Choose the flight from the dropdown
		Select flight = new Select(driver.findElement(By.id("flight")));
		flight.selectByVisibleText("Vistara102");
	    logger1.log(Status.PASS, " Flight selected from dropdown");
	    logger1.log(Status.INFO, " Selected Flight ");
	    
	    
	    //Test Case3 : Choose the route from the dropdown
	    Select Route = new Select(driver.findElement(By.id("route")));
		Route.selectByIndex(6);
        logger1.log(Status.PASS, " Flight selected from dropdown");
	    logger1.log(Status.INFO, " Selected Flight ");
	    	    
	  //Test Case4 : Enter the distance 
	    driver.findElement(By.id("distance")).sendKeys("18400");
	    logger1.log(Status.PASS, " Enter the distance");
	    logger1.log(Status.INFO, " valid Distance ");
	    Thread.sleep(5000);
	    //Test Case5: enter Departure Date
	    driver.findElement(By.id("departureDate")).click();
	    driver.findElement(By.xpath("//*[@id=\"AddSchedule\"]/dl/dd[6]/img")).click();
	    driver.findElement(By.partialLinkText("10")).click();
	    logger1.log(Status.PASS, " Enter the departure date");
	    logger1.log(Status.INFO, " Valid date ");
	    Thread.sleep(5000);
	    
	    //Test Case6 : Choose the arrival time from the dropdown
	    Select DepartureTime = new Select(driver.findElement(By.id("departureTime")));
	    DepartureTime.selectByIndex(5);
        logger1.log(Status.PASS, " Departure Time  selected from dropdown");
	    logger1.log(Status.INFO, "Departure Time Selected ");
	    Thread.sleep(5000);
	    
	    //Test Case7 :enter  valid Arrival Date 
	    driver.findElement(By.id("arrivalDate")).click();
	    driver.findElement(By.xpath("//*[@id=\"AddSchedule\"]/dl/dd[8]/img")).click();
	    driver.findElement(By.partialLinkText("20")).click();
	    logger1.log(Status.PASS, " Enter the arrival date");
	    logger1.log(Status.INFO, " Valid  arrival date ");
	    Thread.sleep(5000);
	  
	    
	    //Test Case8: Choose the arrival time from the dropdown
	    Select ArrivalTime = new Select(driver.findElement(By.id("arrivalTime")));
	    ArrivalTime.selectByIndex(5);
        logger1.log(Status.PASS, " Arrival Time selected from dropdown");
	    logger1.log(Status.INFO, " Arrival Time Selected ");
	    Thread.sleep(5000);
	     
	  // Test Case 9 : Enter the Economy cost
        driver.findElement(By.id("classEconomy")).sendKeys("1000");
	    logger1.log(Status.PASS, " Enter the Economy Cost ");
	    logger1.log(Status.INFO, " Cost added Successfully ");
	    Thread.sleep(5000);
	    
	    //Test Case10: Clicking Add Button
	    driver.findElement(By.id("signInButton")).click();
	    logger1.log(Status.PASS, "Clicking Add Button");
	    logger1.log(Status.INFO, " Add Button Clicked ");
	    Thread.sleep(5000);

	    
		File src = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
		
		try {
			FileUtils.copyFile(src, new File("C://Users//Neha//eclipse-workspace//Automation//AutomationHappyTrip//screenshots//screen.png"));
			
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		extent.flush();
	}
	@AfterSuite
	public void tearDown() {
		extent.flush();
	}
}
